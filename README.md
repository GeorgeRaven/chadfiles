ChadFiles
=========

These managed files setup an i3-gaps environment.
These files are particularly geared towards setting up my cozy Neovim setup.

Requirements
------------

Requirements (archlinux) for a working setup:

- chezmoi [Configuration Manager]
- make [Installation Helper]
- i3 (gaps) [Window Manager]
- polybar [Status Bar]
- neovim [Editor / IDE]
- dunst [Notifications]
- rofi [Menu]
- feh [Static Background] (uses `~/wall.*` file)
- noto fonts for charsets (all of noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra)

Further requirements for neovim and plugins (use :checkhealth to confirm):
- python-neovim [CoC, VimSpector]
- python-pyx [CoC, Vimspector]
- python-isort [CoC]
- black [CoC]
- Go [Vim-Go]

Installation
------------

First create a configuration file in ``~/.config/chezmoi/chezmoi.toml``, here is a basic starting point:

```toml

[data]
email="<YOUR_GITHUB_EMAIL>"
username="<YOUR_GITHUB_USERNAME>"
signingkey="<YOUR_GPG_SIGNING_KEY_ID>"

```

To install this chezmoi repository, run:

```bash
chezmoi init ssh://git@gitlab.com/georgeraven/chadfiles.git
chezmoi apply
```

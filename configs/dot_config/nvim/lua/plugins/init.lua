local sup_langs = {"go", "python", "dockerfile", "lua", "html", "javascript", "typescript", "svelte"}

local plugins = {
  {
    "stevearc/conform.nvim",
    config = function()
      require "configs.conform"
    end,
  },

  {
    "nvim-tree/nvim-tree.lua",
    opts = {
      git = { enable = true },
    },
  },

  -- Grafana Alloy syntax highlighting
  -- https://github.com/grafana/vim-alloy
  {
    "grafana/vim-alloy",
    branch = "main",
    lazy = true,
    ft = { "alloy" },
  },

  -- Conqueror of Completion (Autocompletion)
  -- https://github.com/neoclide/coc.nvim
  {
    "neoclide/coc.nvim",
    branch = "release",
    lazy = false,
  },

  -- Comment helper
  -- https://github.com/numToStr/Comment.nvim
  {
    "numToStr/Comment.nvim",
    lazy = false,
  },

  -- Visual multiple cursors like intelij and vscode
  -- https://github.com/mg979/vim-visual-multi
  {
    "mg979/vim-visual-multi",
    lazy = false,
  },

  -- Markdown Preview
  -- https://github.com/iamcco/markdown-preview.nvim
  {
    "iamcco/markdown-preview.nvim",
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    lazy = true,
    ft = { "markdown" },
    build = function() vim.fn["mkdp#util#install"]() end,
  },

  -- Codeium AI (Autocompletion)
  -- https://github.com/Exafunction/codeium.nvim
  -- https://github.com/Exafunction/codeium.vim
  {
    "Exafunction/codeium.vim",
    event = "InsertEnter",
    lazy = true,
    ft = sup_langs,
    config = function ()
      vim.g.codeium_disable_bindings = 1
      vim.keymap.set("i", "<A-m>", function() return vim.fn["codeium#Accept"]() end, { expr = true })
      vim.keymap.set("i", "<A-f>", function() return vim.fn["codeium#CycleCompletions"](1) end, { expr = true })
      vim.keymap.set("i", "<A-b>", function() return vim.fn["codeium#CycleCompletions"](-1) end, { expr = true })
      vim.keymap.set("i", "<A-x>", function() return vim.fn["codeium#Clear"]() end, { expr = true })
      vim.keymap.set("i", "<A-s>", function() return vim.fn["codeium#Complete"]() end, { expr = true })
    end,
  },

  -- Ollama Nvim
  -- https://github.com/nomnivore/ollama.nvim
  {
    "nomnivore/ollama.nvim",
    dependencies = {"nvim-lua/plenary.nvim",},
    lazy = false,
    cmd = {"Ollama", "OllamaModel", "OllamaServe", "OllamaServeStop"},
    keys = {
      -- Sample keybind for prompt menu. Note that the <c-u> is important for selections to work properly.
      {
        "<leader>oo",
        ":<c-u>lua require('ollama').prompt()<cr>",
        desc = "ollama prompt",
        mode = { "n", "v" },
      },
      -- Sample keybind for direct prompting. Note that the <c-u> is important for selections to work properly.
      {
        "<leader>oG",
        ":<c-u>lua require('ollama').prompt('Generate_Code')<cr>",
        desc = "ollama Generate Code",
        mode = { "n", "v" },
      },
    },
    opts = {
      model = "mistral",
      url = "http://127.0.0.1:11434",
      serve = {
        on_start = false,
        command = "ollama",
        args = { "serve" },
        stop_command = "pkill",
        stop_args = { "-SIGTERM", "ollama" },
      },
      -- View the actual default prompts in ./lua/ollama/prompts.lua
      prompts = {
        Sample_Prompt = {
          prompt = "This is a sample prompt that receives $input and $sel(ection), among others.",
          input_label = "> ",
          model = "mistral",
          action = "display",
        }
      }
    }
  },


  -- git-conflict
  -- https://github.com/akinsho/git-conflict.nvim
  {
    "akinsho/git-conflict.nvim",
    version = "*",
    lazy = false,
    config = {
      default_mappings = true,
      default_commands = true,
      disable_diagnostics = false,
      highlights = {
        incoming = "DiffText",
        current = "DiffAdd",
      },
    },
  },

  -- black official plugin
  -- this is needed for some autocmds
  -- https://github.com/psf/black
  {
    "psf/black",
    lazy = true,
    ft = { "python" },
  },

  -- isort
  -- this is needed for some autocmds
  -- https://github.com/fisadev/vim-isort
  --{
  --  "fisadev/vim-isort",
  --  lazy = true,
  --  ft = { "python" },
  --},

  -- Vim-Go (language support for Go)
  -- https://github.com/fatih/vim-go
  {
    "fatih/vim-go",
    lazy = true,
    ft = {"go"}
  },

  -- Vim-Sleuth (Indentation Helper)
  -- https://github.com/tpope/vim-sleuth
  {
    "tpope/vim-sleuth",
    lazy = true,
    after = "editorconfig-vim",
  },

  -- Notify
  -- https://github.com/rcarriga/nvim-notify
  {
    "rcarriga/nvim-notify",
    lazy = false,
    config = function ()
      vim.notify = require("notify")
    end
  },

  -- nvim-dap
  -- https://github.com/mfussenegger/nvim-dap
  {
    "mfussenegger/nvim-dap",
    lazy = true,
    ft = sup_langs,
  },

  -- mason-nvim-dap
  -- https://github.com/jay-babu/mason-nvim-dap.nvim
  {
    "jay-babu/mason-nvim-dap.nvim",
    lazy = true,
    ft = sup_langs,
    config = function ()
      require("mason-nvim-dap").setup({
        ensure_installed = { "python", "delve" },
      })

      local dap_ok, dap = pcall(require, "dap")
      if not (dap_ok) then
        print("nvim-dap not installed!")
        return
      end
      require('dap').set_log_level('INFO') -- Helps when configuring DAP, see logs with :DapShowLog
      dap.configurations = {
          go = {
            {
              type = "go", -- Which adapter to use
              name = "Debug", -- Human readable name
              request = "launch", -- Whether to "launch" or "attach" to program
              program = "${file}", -- The buffer you are focused on when running nvim-dap
            },
          }
      }
      dap.adapters.go = {
        type = "server",
        port = "${port}",
        executable = {
          command = vim.fn.stdpath("data") .. '/mason/bin/dlv',
          args = { "dap", "-l", "127.0.0.1:${port}" },
        },
      }



    end
  },

  -- Vimspector (Debugger)
  -- https://github.com/puremourning/vimspector
  --{
  --  "puremourning/vimspector",
  --  -- branch = "master",
  --  lazy = false,
  --  ft = sup_langs,
  --},
}

return plugins

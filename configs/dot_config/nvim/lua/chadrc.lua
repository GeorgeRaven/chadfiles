local M = {}

M.options = {
  clipboard = "unnamedplus",
}

M.ui = {
  theme = "solarized_dark",
  theme_toggle = { "solarized_dark", "one_light" },
}

return M

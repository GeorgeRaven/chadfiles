require "nvchad.mappings"

-- add yours here

local map = vim.keymap.set

map("n", ";", ":", { desc = "CMD enter command mode" })

map("n", "<leader>fm", function()
  require("conform").format()
end, { desc = "File Format with conform" })

map("i", "jk", "<ESC>", { desc = "Escape insert mode" })

map("n", "<C-Bslash>", "<cmd>NvimTreeToggle<CR>", { desc = "Nvimtree Toggle window" })

map("n", "<leader>dt", "<cmd>lua require'dap'.toggle_breakpoint()<CR>", { desc = "Nvim DAP toggle breakpoint" })

map("n", "<leader>dd", "<cmd>lua require'dap'.continue()<CR>", { desc = "Nvim DAP continue" })

map("n", "<leader>de", "<cmd>lua require'dap'.terminate()<CR>", { desc = "Nvim DAP terminate" })

map("n", "<leader>dn", "<cmd>lua require'dap'.step_over()<CR>", { desc = "Nvim DAP step over" })

map("n", "<leader>di", "<cmd>lua require'dap'.step_into()<CR>", { desc = "Nvim DAP step into" })

map("n", "<leader>do", "<cmd>lua require'dap'.step_out()<CR>", { desc = "Nvim DAP step out" })

map("n", "<leader>tn", "<cmd>lua require'telescope'.extensions.notify.notify{}<CR>", { desc = "Show notification history" })

map("n", "<leader>fd", ":call CocAction('jumpDefinition')<CR>", { desc = "Go to definition" })
--map("n", "<leader>fd", "<cmd>lua require('telescope.builtin').lsp_definitions()<CR>", { desc = "Go to definition" })

--M.general = {
--  n = {
--    [";"] = { ":", "enter command mode", opts = { nowait = true } },
--    ["<C-_>"] = { ":lua require('Comment.api').toggle_current_linewise()<CR>", "toggle line comment", opts = { nowait = true } },
--    ["<C-Bslash>"] = { "<cmd>NvimTreeToggle<CR>", "toggle file tree", opts = { nowait = true } },
--		--- VimSpector
--		["<leader>dd"] = { ":call vimspector#Launch()<CR>", "Launch vimspector", opts = { nowait = true } },
--		["<leader>de"] = { ":call vimspector#Reset()<CR>", "Reset vimspector", opts = { nowait = true } },
--		["<leader>dc"] = { ":call vimspector#Continue()<CR>", "Continue vimspector", opts = { nowait = true } },
--		["<leader>dt"] = { ":call vimspector#ToggleBreakpoint()<CR>", "Toggle breakpoint", opts = { nowait = true } },
--		["<leader>dT"] = { ":call vimspector#ClearBreakpoints()<CR>", "Clear breakpoints", opts = { nowait = true } },
--		["<leader>dk"] = { "<Plug>VimspectorRestart<CR>", "Restart vimspector", opts = { nowait = true } },
--		["<leader>dh"] = { "<Plug>VimspectorStepOut<CR>", "Debugger step out", opts = { nowait = true } },
--		["<leader>dl"] = { "<Plug>VimspectorStepInto<CR>", "Debugger step into", opts = { nowait = true } },
--		["<leader>dj"] = { "<Plug>VimspectorStepOver<CR>", "Debugger step over", opts = { nowait = true } },
--		--- CoC
--		["<leader>fd"] = { ":call CocAction('jumpDefinition')<CR>", "Go to definition", opts = { nowait = true } },
--  },
--  v = {
--    ["<C-_>"] = { ":lua require('Comment.api').toggle_linewise_op(vim.fn.visualmode())<CR>", "toggle visual block comment", opts = { nowait = true } },
--  },

if status is-interactive
    # Commands to run in interactive sessions can go here
end

alias k 'kubectl'
alias t 'talosctl --talosconfig=./talosconfig'
alias c 'cilium-cli -n cilium'

set fish_greeting

REPO=ssh://git@gitlab.com/georgeraven/chadfiles.git
# every double comment will be used to auto generate help messages

.PHONY: help
help: ## display this auto generated help message
	@echo "Please provide a make target:"
	@grep -F -h "##" $(MAKEFILE_LIST) | grep -F -v grep -F | sed -e 's/\\$$//' | sed -e 's/##//'

.PHONY: init
init: ## initialise chezmoi source to use https://gitlab.com/GeorgeRaven/chadfiles.git
	chezmoi init ${REPO}

.PHONY: install
install: ## install currentley pulled source version (no update)
	chezmoi apply

.PHONY: update
update: ## reconcile local source with remote source
	chezmoi git pull

.PHONY: status
status: ## show what would be applied with apply
	chezmoi status

.PHONY: diff
diff: ## show diff between local source and current configuration
	chezmoi diff

.PHONY: merge
merge: ## merge local source with currently running configuration
	chezmoi merge

.PHONY: upgrade
upgrade: update install ## reconcile against remote and install

# .PHONY: uninstall
# uninstall: ## uninstall configuration installed by this utility
# 	chezmoi
